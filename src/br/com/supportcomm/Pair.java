package br.com.supportcomm;


public class Pair<A, B> {

    public final A first;
    public final B second;

    public Pair(A a, B b) {
        this.first = a;
        this.second = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair<?, ?> tuple = (Pair<?, ?>) o;
        if (!first.equals(tuple.first)) return false;
        return second.equals(tuple.second);
    }

}