package br.com.supportcomm;

public class DetalheChamadas {
	private String msisdn;
	private String numChamadas;	
	private String retornoIvrBlock;
	
	
	public DetalheChamadas()
	{
		
	}
	
	public void setMsisdn(String _msisdn){
		msisdn = _msisdn;
	}
	public String getMsisdn() {
		return msisdn;
	}
	
	public void setNumChamadas(String _numChamadas) {
		numChamadas = _numChamadas;
	}
	public String getNumChamadas() {
		return numChamadas;
	}
	
	public void setRetornoIvrBlock(String _retornoIvrBlock) {
		retornoIvrBlock = _retornoIvrBlock;
	}
	public String getRetornoIvrBlock() {
		return retornoIvrBlock;
	}	
	
}

