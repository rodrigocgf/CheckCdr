package br.com.supportcomm;

public interface IConfig {	
	public String getDbUser();
	public String getDbPassword();
	public String getDbIpAddress();
	public String getDatabasename();
	public String getMinutesFormer();
	public String getNumExcessiveCalls();
	public String getIntervalBetweenChecks();
	public String getServiceCode();
	public String getCallId();
	public String getChannel();
}
