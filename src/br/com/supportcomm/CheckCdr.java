package br.com.supportcomm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Queue;
import java.util.Timer;
import java.util.concurrent.LinkedBlockingQueue;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Servlet implementation class CheckCdr
 */
@WebServlet(name = "CheckCdr", urlPatterns = "/Check" )
public class CheckCdr extends HttpServlet implements ICheckCdr{
	private static final long serialVersionUID = 1L;			
	private Logger logger = null;
	private long intervalBetweenChecks = 0;
	
	//public static String LOG4JPROPERTIES = "C:\\PROJETOS\\Java\\CheckCdr\\resources\\log4j.properties";	
	//private static String CONFIG_DIR = "C:\\PROJETOS\\Java\\CheckCdr\\resources\\";
	
	private static String LOG4JPROPERTIES = "/var/supportcomm/CheckCdr/config/log4j.properties";
	private static String CONFIG_DIR = "/var/supportcomm/CheckCdr/config/";
	
	ArrayList<String> listPropertiesFiles = null;	
	private Queue<Pair<Config, ArrayList<DetalheChamadas> > > queueConfigCachesDetalheChamadas;
	private Map<Integer, MonitorCacheThr> mapTask2Thread;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckCdr() {
        super();
        // TODO Auto-generated constructor stub
        queueConfigCachesDetalheChamadas = new LinkedBlockingQueue<Pair<Config, ArrayList<DetalheChamadas> > >(); 
        mapTask2Thread = new HashMap<Integer, MonitorCacheThr>();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);	
		
		
		String _configDir = CONFIG_DIR;
		listPropertiesFiles = new ArrayList<String>();
		String log4JPropertyFile = LOG4JPROPERTIES;
		Properties p = new Properties();
		
		
		try {
		    p.load(new FileInputStream(log4JPropertyFile));
		    PropertyConfigurator.configure(p);
		    logger = Logger.getLogger("CheckCdr");		    
		} catch (IOException e) {
			e.printStackTrace();
		}

		logger.debug("=========================================================");
		logger.debug("init CheckCdr ...");
		
		listPropertiesFiles = searchForCheckCdrPropertiesFiles(_configDir);
		
		if ( listPropertiesFiles != null ) {
			int _taskNum = 1;
			for (String configFileName : listPropertiesFiles) {
				StringBuilder sbConfigFullPath = new StringBuilder();
				ArrayList<DetalheChamadas> cacheDetalheChamadas = null;
				DBCdrTask dbCdrTask = null;
				Config _config = null;
				Timer timer = null;
				MonitorCacheThr monitorCacheThr = null;				
				
				sbConfigFullPath.append(_configDir);
				sbConfigFullPath.append(configFileName);
				
				_config = new Config(sbConfigFullPath.toString());
				cacheDetalheChamadas = new ArrayList<DetalheChamadas>();
				dbCdrTask = new DBCdrTask(_taskNum, this , _config , cacheDetalheChamadas );				
				timer = new Timer(true);
				
				intervalBetweenChecks = Integer.parseInt(_config.getIntervalBetweenChecks());
				timer.scheduleAtFixedRate(dbCdrTask , 0, intervalBetweenChecks);
				
				monitorCacheThr = new MonitorCacheThr( _config , cacheDetalheChamadas);				
				
				Pair<Config, ArrayList<DetalheChamadas> > _pair = 
						new Pair<Config, ArrayList<DetalheChamadas> >(_config, cacheDetalheChamadas);
				
				queueConfigCachesDetalheChamadas.add(_pair);
				mapTask2Thread.put(_taskNum, monitorCacheThr);
				
				monitorCacheThr.start();
				_taskNum++;
			}
		}
		
		
	}

	public void notifyMonitorThread(int taskNum) {
		
		if ( mapTask2Thread.containsKey(taskNum))
		{
			MonitorCacheThr _monitorCacheThr = mapTask2Thread.get(taskNum);
			
			if ( _monitorCacheThr != null)
				_monitorCacheThr.notifyExecution();
		
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		for (  Entry<Integer, MonitorCacheThr> entry : mapTask2Thread.entrySet() ) {		    
		    MonitorCacheThr monitorCacheThr = entry.getValue();
		    
		    monitorCacheThr.terminate();
		    
		    try {
				monitorCacheThr.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		logger.debug("destroy CheckCdr ...");
		logger.debug("=========================================================");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		
		response.setStatus(HttpServletResponse.SC_OK);		
		
		for ( Pair pair : queueConfigCachesDetalheChamadas ) {
			Config _config = (Config) pair.first;
			ArrayList<DetalheChamadas> _cacheDetalheChamadas = (ArrayList<DetalheChamadas>) pair.second;
			
			printHTML(response, _config , _cacheDetalheChamadas);
		}
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	private ArrayList<String> searchForCheckCdrPropertiesFiles(String _propDir) {
		ArrayList<String> listRet = new ArrayList<String>();
		
		File dir = new File(_propDir);
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept (File dir, String name) { 
				return name.startsWith("CheckCdr.properties");
			} 
		}; 
		logger.debug("Searching for properties files at " + _propDir );
		String[] children = dir.list(filter);
		if (children == null) {
			//System.out.println("Either dir does not exist or is not a directory");
			logger.debug("Either dir does not exist or is not a directory");
		} else { 
			for (int i = 0; i< children.length; i++) {
				String filename = children[i];
				logger.debug(" * PROPERTY FILE : " + filename);
				listRet.add(filename);
			} 
		}
		logger.debug(" ");
		
		return listRet;
	}
	
	private void printHTML(HttpServletResponse response, Config config , ArrayList<DetalheChamadas> _cacheDetalheChamadas) {
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		sb.append("<head>");
		sb.append("</head>");
		sb.append("<h2>CHECAGEM PERIODICA DE CDR's - MSISDN's COM NUMERO EXCESSIVO DE LIGACOES</h2>");
		sb.append("<h3> Banco de dados : ");
		sb.append(config.getDbIpAddress() );
		sb.append(":");
		sb.append(config.getDatabasename());
		//sb.append("</h3>");		
		//sb.append("<h3> Periodo consultado no banco de dados: de ");
		//sb.append(dbCdrTask.getDtEnd());
		//sb.append(" a ");
		//sb.append(dbCdrTask.getDtStart());
		//sb.append("</h3>");
		sb.append("<h3> Numero de chamadas excessivas : ");
		sb.append(config.getNumExcessiveCalls());
		sb.append("</h3>");
		sb.append("<h3> Intervalo entre checagens sucessivas (ms): ");
		sb.append(config.getIntervalBetweenChecks());
		sb.append("</h3>");
		sb.append("<table>");
		sb.append("<th style = \"background: #333; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: left;\">      MSISDN        ");
		sb.append("</th>");
		sb.append("<th style = \"background: #333; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: left;\"> NUMERO DE CHAMADAS  ");
		sb.append("</th>");
		sb.append("<th style = \"background: #333; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: left;\"> RETORNO DO IVR_BLOCK  ");
		sb.append("</th>");
		

		for (DetalheChamadas det : _cacheDetalheChamadas) {
		    sb.append("<tr>");
		    sb.append("<td style = \"padding: 6px; border: 1px solid #ccc; text-align: left;\"> " + det.getMsisdn());
		    sb.append("</td>");
		    sb.append("<td style = \"padding: 6px; border: 1px solid #ccc; text-align: left;\"> " + det.getNumChamadas());
		    sb.append("</td>");
		    sb.append("<td style = \"padding: 6px; border: 1px solid #ccc; text-align: left;\"> " + det.getRetornoIvrBlock());
		    sb.append("</td>");
		    sb.append("</tr>");
		}
		sb.append("</table>");
		sb.append("</body>");
		sb.append("</html>");
		out.println(sb.toString());

	}
	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
