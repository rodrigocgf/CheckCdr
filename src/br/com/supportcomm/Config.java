package br.com.supportcomm;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletConfig;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Config implements IConfig {
	private Logger logger = null;
	
	//public static String LOG4JPROPERTIES = "C:\\PROJETOS\\Java\\CheckCdr\\resources\\log4j.properties";
	//private static String CONFIG_FILE = "C:\\PROJETOS\\Java\\CheckCdr\\resources\\CheckCdr.properties";
	
	private static String LOG4JPROPERTIES = "/var/supportcomm/CheckCdr/config/log4j.properties";	
	private static String CONFIG_FILE = "/var/supportcomm/CheckCdr/config/CheckCdr.properties";
	
	private Properties prop = new Properties();
	
	public Config() {
		String log4JPropertyFile = LOG4JPROPERTIES;
		Properties p = new Properties();
		
		try {
		    p.load(new FileInputStream(log4JPropertyFile));
		    PropertyConfigurator.configure(p);
		    logger = Logger.getLogger("CheckCdr");		    
		} catch (IOException e) {		
			logger.debug(e.toString());
			e.printStackTrace();
		}
		
		
		readCheckCdrCfgFile(CONFIG_FILE);
	}
	
	public Config(String _cfgFileName) {
		String log4JPropertyFile = LOG4JPROPERTIES;
		Properties p = new Properties();
		
		try {
		    p.load(new FileInputStream(log4JPropertyFile));
		    PropertyConfigurator.configure(p);
		    logger = Logger.getLogger("CheckCdr");		    
		} catch (IOException e) {		
			logger.debug(e.toString());
			e.printStackTrace();
		}
		
		readCheckCdrCfgFile(_cfgFileName);
	}
	
	public Config(boolean dev) {
		setDefaultConfig();
	}
	
	public Config(ServletConfig cfg) {
		String log4JPropertyFile = LOG4JPROPERTIES;
		Properties p = new Properties();
		
		try {
		    p.load(new FileInputStream(log4JPropertyFile));
		    PropertyConfigurator.configure(p);
		    logger = Logger.getLogger("CheckCdr");		    
		} catch (IOException e) {
			logger.debug(e.toString());
			e.printStackTrace();
		}		
		
		parseServletConfigFromWebXml(cfg);
		//setDefaultConfig();
	}
	
	private void readCheckCdrCfgFile(String _cfgFileName) {		
		InputStream input = null;
		
		try {
			input = new FileInputStream(_cfgFileName);
			prop.load(input);
			logger.debug("PROPERTIES READ FROM " + _cfgFileName + " : ");			
						
			logger.debug("dbUser : " + prop.getProperty("dbUser"));
			logger.debug("dbPassword : " + prop.getProperty("dbPassword"));
			logger.debug("dbIpAddress : " + prop.getProperty("dbIpAddress"));
			logger.debug("databasename : " + prop.getProperty("databasename"));
			logger.debug("minutesFormer : " + prop.getProperty("minutesFormer"));
			logger.debug("numExcessiveCalls : " + prop.getProperty("numExcessiveCalls"));
			logger.debug("intervalBetweenChecks : " + prop.getProperty("intervalBetweenChecks"));
			logger.debug("serviceCode : " + prop.getProperty("serviceCode"));
			logger.debug("callId : " + prop.getProperty("callId"));
			logger.debug("channel : " + prop.getProperty("channel"));

		} catch (IOException ex) {
			logger.debug(ex.toString());
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					logger.debug(e.toString());
					e.printStackTrace();
				}
			}
		}
	}
	
	private void parseServletConfigFromWebXml(ServletConfig cfg) {
		Enumeration<String> listParamNames = cfg.getInitParameterNames();
		
		if ( !listParamNames.hasMoreElements() )
			return;	
				
		prop.setProperty("dbUser",cfg.getInitParameter("dbUser"));
		prop.setProperty("dbPassword", cfg.getInitParameter("dbPassword"));
		prop.setProperty("dbIpAddress", cfg.getInitParameter("dbIpAddress"));
		prop.setProperty("databasename", cfg.getInitParameter("databasename"));
		prop.setProperty("minutesFormer" , cfg.getInitParameter("minutesFormer"));
		prop.setProperty("numExcessiveCalls" , cfg.getInitParameter("numExcessiveCalls"));
		prop.setProperty("intervalBetweenChecks" , cfg.getInitParameter("intervalBetweenChecks"));
		prop.setProperty("serviceCode" , cfg.getInitParameter("serviceCode"));
		prop.setProperty("callId" , cfg.getInitParameter("callId"));
		prop.setProperty("channel" , cfg.getInitParameter("channel"));
	}
	
	private void setDefaultConfig() {
		prop.setProperty("dbUser","sa");
		prop.setProperty("dbPassword","ScommVivo");
		prop.setProperty("dbIpAddress","192.168.3.99");
		prop.setProperty("databasename","db_vivo2010");
		prop.setProperty("minutesFormer","5000");
		prop.setProperty("numExcessiveCalls","5");
		prop.setProperty("intervalBetweenChecks","1800000");
		prop.setProperty("serviceCode","2020");
		prop.setProperty("callId","8109");
		prop.setProperty("channel","OUTROS");
	}
	
	public String getDbUser() {
		return prop.getProperty("dbUser");
	}
	
	public String getDbPassword() {
		return prop.getProperty("dbPassword");
	}
	
	public String getDbIpAddress() {
		return prop.getProperty("dbIpAddress");
	}
	
	public String getDatabasename() {
		return prop.getProperty("databasename");
	}
	
	public String getMinutesFormer() {
		return prop.getProperty("minutesFormer");
	}
	
	public String getNumExcessiveCalls() {
		return prop.getProperty("numExcessiveCalls");
	}
	
	public String getIntervalBetweenChecks() {
		return prop.getProperty("intervalBetweenChecks");
	}
	
	public String getServiceCode() {
		return prop.getProperty("serviceCode");
	}
	
	public String getCallId() {
		return prop.getProperty("callId");
	}
	
	public String getChannel() {
		return prop.getProperty("channel");
	}
}
