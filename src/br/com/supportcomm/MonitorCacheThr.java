package br.com.supportcomm;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Queue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



public class MonitorCacheThr extends Thread {
	public int threadNum;
	private volatile boolean running = true;
	public ArrayList<DetalheChamadas> listDetalheChamadas = null;
	private final String STRBLOCKMSISDN = "http://bloqueio.dc1.supp.com.br/ServiceBlock/BlockService?msisdn=%s&servicecode=%s&channel=%s&callid=%s";
	private final String STRCHECKBLOCKMSISDN = "http://bloqueio.dc1.supp.com.br/ServiceBlock/CheckBlock?msisdn=%s&servicecode=%s";
	private volatile boolean notifyToExecute = false;
	private final String USER_AGENT = "Mozilla/5.0";	
	private IConfig config = null;	
	private Logger logger = null;	
	
	private static String LOG4JPROPERTIES = "/var/supportcomm/CheckCdr/config/log4j.properties";
	//private static String LOG4JPROPERTIES = "C:\\PROJETOS\\Java\\CheckCdr\\resources\\log4j.properties";
	
	XPathFactory xpathFactory = null;
    XPath xpath = null;
    XPathExpression expr = null;

	public MonitorCacheThr( IConfig _config , ArrayList<DetalheChamadas> _listDetalheChamadas) {	
		
		String log4JPropertyFile = LOG4JPROPERTIES;
		Properties p = new Properties();
		
		try {
		    p.load(new FileInputStream(log4JPropertyFile));
		    PropertyConfigurator.configure(p);
		    logger = Logger.getLogger("CheckCdr");		    
		} catch (IOException e) {
			e.printStackTrace();
		}		

		logger.debug("=========================================================");
		logger.debug("MonitorCacheThr ctor()");
		
		config = _config;
		listDetalheChamadas = _listDetalheChamadas;
		
		xpathFactory = XPathFactory.newInstance();
	    xpath = xpathFactory.newXPath();

	}
	
	public MonitorCacheThr( Queue<Config> _queueConfig , Queue<ArrayList> _queueCachesDetatlheChamadas) {
		
	}

	public synchronized void notifyExecution() {
		notifyToExecute = true;
	}
	
	public synchronized void stopNotificationToExecution() {
		notifyToExecute = false;
	}

	public synchronized boolean verifyNotification() {
		return notifyToExecute;
	}

	public void terminate() {
		running = false;
	}
	
	public void run() {
		StringBuilder sb = new StringBuilder();
		String retornoIvrBlock = "";
		Pair<String,String> t_isIvrBlocked = null;

		while ( running ) {
			if ( verifyNotification() ) {
				logger.debug("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
				//System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
				for (DetalheChamadas det : listDetalheChamadas) {
					sb.setLength(0);
					sb.append("{");
					sb.append(det.getMsisdn());
					sb.append(":");
					sb.append(det.getNumChamadas());
					sb.append("}");					
					//System.out.println(sb.toString());					
					
					t_isIvrBlocked = isIvrBlocked(det.getMsisdn());					
										
					if ( t_isIvrBlocked.first.equals("false")) {
						retornoIvrBlock = callIvrBlock(det.getMsisdn());
						logger.debug("[IVR_BLOCK] : " + retornoIvrBlock);
						det.setRetornoIvrBlock(retornoIvrBlock);
					} else {
						logger.debug("[IVR_BLOCK] : " + t_isIvrBlocked.second);
						det.setRetornoIvrBlock(t_isIvrBlocked.second);
					}
					
				}
				stopNotificationToExecution();
				logger.debug("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
				//System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private Pair<String, String> isIvrBlocked(String _msisdn) {
		Pair<String,String> p_Ret = null;
		String b_Ret = "false";	
		
		URL url;
		HttpURLConnection con = null;		
		String inputLine;
		StringBuffer content = new StringBuffer();
		String description = "";
	    String blocked = "";
	    String sContent = "";
		
		try {			
			
			String sUrl = "";			
			sUrl = String.format(STRCHECKBLOCKMSISDN , _msisdn , config.getServiceCode()  );
			url = new URL(sUrl);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setConnectTimeout(5000);
			con.setReadTimeout(5000);
			
			logger.debug("[IVR_BLOCK] : " + sUrl);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.debug(e.toString());
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			con.setDoOutput(true);			
			
			BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
			
			while ((inputLine = in.readLine()) != null) {
			    content.append(inputLine);
			}
			printHTTPResponse(content);
			sContent = parseIvrXmlResponse(content.toString());
			
		    try {
		    	InputSource inputXML = new InputSource( new StringReader( content.toString() ) );		    		    	
		    	description = (String) xpath.evaluate("/response/body/description", inputXML);		    	
		    	logger.debug("description : " + description);
		    	
		    	InputSource inputXML1 = new InputSource( new StringReader( content.toString() ) );
		    	blocked = (String) xpath.evaluate("/response/body/blocked", inputXML1);
		    	logger.debug("blocked : " + blocked);
		    } catch (Exception e) {
		    	
		    	logger.debug( e.toString());
		        e.printStackTrace();
		    }
			
			in.close();
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug(e.toString());
			e.printStackTrace();
		}
		
		if ( description.equals("Successful")) {
			if ( blocked.equals("true")) {
				logger.debug("MSISDN " + _msisdn + " is already blocked !");
				b_Ret = "true";
			} else {
				logger.debug("MSISDN " + _msisdn + " is NOT blocked !");
			}
		}
						
		p_Ret = new Pair<String,String>(b_Ret, sContent);		
			
		return p_Ret;
	}
	
	private String callIvrBlock(String _msisdn) {
		String strRet = "";
		URL url;
		HttpURLConnection con = null;		
		String inputLine;
		StringBuffer content = new StringBuffer();
		
		try {			
			
			String sUrl = "";			
			sUrl = String.format(STRBLOCKMSISDN , _msisdn , config.getServiceCode() , config.getChannel() , config.getCallId() );
			url = new URL(sUrl);
			con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setConnectTimeout(5000);
			con.setReadTimeout(5000);
			
			logger.debug("[IVR_BLOCK] : " + sUrl);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.debug(e.toString());
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug(e.toString());
			e.printStackTrace();
		}
		
		
		try {
			con.setDoOutput(true);			
			
			BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
			
			while ((inputLine = in.readLine()) != null) {
			    content.append(inputLine);
			}
			printHTTPResponse(content);
			strRet = parseIvrXmlResponse(content.toString());
			in.close();
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug(e.toString());
			e.printStackTrace();
		}		
		
		return strRet;
		//return content.toString();
	}
	
	
	public void printHTTPResponse(StringBuffer content) {
		//System.out.println(content.toString());
		logger.debug(content.toString());
	}
		
	
	private String parseIvrXmlResponse(String _xmlText) {
		StringBuilder sb = new StringBuilder();
		String value = "";
		String name = "";
		
		try {
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse( new InputSource(new StringReader(_xmlText ) ) );
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element elem = (Element) node;						
					
					name = node.getNodeName();
					
					if (name.equals("status")) {
						value = getString("status", elem);
						sb.append("status : ");
						sb.append(value);
						sb.append("\r\n");
						//System.out.println("status : " + value);
					} else if ( name.equals("body") ) {
						NodeList nodeList1 = node.getChildNodes();
						
						for (int j = 0; j < nodeList1.getLength(); j++) {
							Node node1 = nodeList1.item(j);
							
							if (node1.getNodeType() == Node.ELEMENT_NODE) {
								Element elem1 = (Element) node1;
								
								name = node1.getNodeName();
								
								if (name.equals("description")) {
									value = getString("description", elem1);
									sb.append("\tdescription : ");
									sb.append(value);
									sb.append("\r\n");
									//System.out.println("\tdescription : " + value);
								} else if (name.equals("blocked")) {
									value = getString("description", elem1);
									sb.append("\tblocked : ");
									sb.append(value);
									sb.append("\r\n");
									//System.out.println("\tblocked : " + value);
								} else if (name.equals("date")) {
									value = getString("date", elem1);
									sb.append("\tdate : ");
									sb.append(value);
									sb.append("\r\n");
									//System.out.println("\tdate : " + value);
								} else if (name.equals("channel")) {
									value = getString("channel", elem1);
									sb.append("\tchannel : ");
									sb.append(value);
									sb.append("\r\n");
									//System.out.println("\tchannel : " + value);
								} else if (name.equals("protocol")) {
									value = getString("protocol", elem1);
									sb.append("\tprotocol : ");
									sb.append(value);
									sb.append("\r\n");
									//System.out.println("\tprotocol : " + value);
								}
							}
						}
						
					}
					
				}
			}

		}
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		catch( SAXException e) {
			e.printStackTrace();
		}
		catch( IOException e ) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	private String getString(String tagName, Element element) {
		DOMImplementationLS lsImpl = (DOMImplementationLS)element.getOwnerDocument().getImplementation().getFeature("LS", "3.0");
	    LSSerializer lsSerializer = lsImpl.createLSSerializer();
        NodeList list = element.getElementsByTagName(tagName);
        if (list != null && list.getLength() > 0) {
            NodeList subList = list.item(0).getChildNodes();

            if (subList != null && subList.getLength() > 0) {
                return subList.item(0).getNodeValue();
            }
        } else {
        	StringBuilder sb = new StringBuilder();
        	NodeList childNodes = element.getChildNodes();
        	for (int i = 0; i < childNodes.getLength(); i++) {
        		sb.append(lsSerializer.writeToString(childNodes.item(i)));
        	}
        	return sb.toString(); 
        }

        return null;
    }
	
}
