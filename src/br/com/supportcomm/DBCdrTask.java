package br.com.supportcomm;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class DBCdrTask extends TimerTask {
	private int iNumLigacoesExcessivas = 0;
	private int iNumMinutosAnteriores = 0;
	ArrayList<DetalheChamadas> listDetalheChamadas;
	ICheckCdr checkCdr;
	IConfig config = null;
	public	 int    taskNum;
	private String sDtStart = "";
	private String sDtEnd = "";	
	private Logger logger = null;
	
	private static String LOG4JPROPERTIES = "/var/supportcomm/CheckCdr/config/log4j.properties";
	//private static String LOG4JPROPERTIES = "C:\\PROJETOS\\Java\\CheckCdr\\resources\\log4j.properties";

	public DBCdrTask(int _taskNum , ICheckCdr _checkCdr , IConfig _config , ArrayList<DetalheChamadas> _listDetalheChamadas ) {
		
		taskNum = _taskNum;
		String log4JPropertyFile = LOG4JPROPERTIES;
		Properties p = new Properties();
		
		try {
		    p.load(new FileInputStream(log4JPropertyFile));
		    PropertyConfigurator.configure(p);
		    logger = Logger.getLogger("CheckCdr");		    
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//logger = Logger.getLogger("CheckCdr");		
		logger.debug("DBCdrTask ctor()");
		
		config = _config;
		iNumLigacoesExcessivas = Integer.parseInt(config.getNumExcessiveCalls());
		iNumMinutosAnteriores = Integer.parseInt(config.getMinutesFormer());
		listDetalheChamadas = _listDetalheChamadas;
		checkCdr = _checkCdr;
	}

	public String getDtStart() {
		return sDtStart;
	}
	
	public String getDtEnd() {
		return sDtEnd;
	}
	
	public void run() {
		StringBuilder sbJdbcUrl = new StringBuilder();
		StringBuilder query = new StringBuilder();

		String jdbcUrl = "";
		String queryFormatted = "";
		String msisdn = "";
		String numChamadas = "";
		
		logger.debug("                                                                    ");
		logger.debug("{{{ ------------------------------------------------------------ }}}");
		
		
		sbJdbcUrl.append("jdbc:jtds:sqlserver://%s:1433;DatabaseName=%s;");
		sbJdbcUrl.append("encrypt=false;trustServerCertificate=false;loginTimeout=30;");
		
		jdbcUrl = String.format(sbJdbcUrl.toString() , config.getDbIpAddress() , config.getDatabasename() );
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
		Calendar cal = Calendar.getInstance();
		
		Date dtStart = new java.util.Date();
		cal.setTime(dtStart);
		cal.add(Calendar.MINUTE, -1*iNumMinutosAnteriores);
		Date dtEnd = cal.getTime();
		
		sDtStart = format1.format(dtStart);
		sDtEnd = format1.format(dtEnd);
		
		//query.append("select des_ani as msisdn, count(*) as numChamadas from dbo.Operation where ");
		//query.append("dat_initial_operation >= '%s' and ");
		//query.append("dat_end_operation <= '%s' ");
		//query.append("group by des_ani having count(*) >= %d");
		query.append("select des_ani as msisdn, count(*) as numChamadas from dbo.Operation where ");
		query.append("dat_end_operation >= '%s' ");		
		query.append("group by des_ani having count(*) >= %d");
		
		queryFormatted = String.format(query.toString() , sDtEnd , iNumLigacoesExcessivas);
	
		try {
			Connection con = null;		
			try {				
				Class.forName("net.sourceforge.jtds.jdbc.Driver");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        con = DriverManager.getConnection(jdbcUrl, config.getDbUser(), config.getDbPassword() );
	        logger.debug("Going to connect to " + jdbcUrl);
            if (con != null) {                
                logger.debug("Connection to " + config.getDatabasename() + " Successful!");
            } else {            	
            	logger.debug("Connection to " + config.getDatabasename() + " NOT Successful!");
            	return;
            }

			Statement stmtObj = con.createStatement();
			ResultSet resObj = stmtObj.executeQuery(queryFormatted);
			logger.debug("Query executed at " + config.getDatabasename() + " : ");
			logger.debug("{");
			logger.debug(queryFormatted);
			logger.debug("}");
			
			listDetalheChamadas.clear();
			while ( resObj.next())
			{
				msisdn = resObj.getString("msisdn");
				numChamadas = resObj.getString("numChamadas");
				DetalheChamadas detalhe = new DetalheChamadas();
				detalhe.setMsisdn(msisdn);
				detalhe.setNumChamadas(numChamadas);
				
				listDetalheChamadas.add(detalhe);
			}
			
			printCache(listDetalheChamadas);
			
			if ( listDetalheChamadas.size() >  0 ) 
				checkCdr.notifyMonitorThread(taskNum);
			
			con.close();
			logger.debug("Connection closed.");
			
			logger.debug("}}} ------------------------------------------------------------ {{{");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void printCache(ArrayList<DetalheChamadas> _listDetalheChamadas) {		
		
		logger.debug("-----------------------------------------");
		logger.debug("|     MSISDN    |     NUMBER OF CALLS   |");
		logger.debug("-----------------------------------------");		
		
		for ( DetalheChamadas det : _listDetalheChamadas) {
			StringBuilder sb = new StringBuilder();
			sb.append("|");
			sb.append(String.format("%14s ",det.getMsisdn() ) );
			sb.append("|");			
			sb.append(String.format("%22s ", det.getNumChamadas() ) );
			sb.append("|");
			logger.debug(sb.toString());
		}
				
		logger.debug("-----------------------------------------");
	}
}
